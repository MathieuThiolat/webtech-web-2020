$(document).ready(function () {
  console.log("In app.js");
  $("#myForm").submit(function () {
    var formData = new FormData(document.querySelector("#myForm"));
    $("form")
      .serializeArray()
      .forEach((element) => {
        sessionStorage.setItem(element.name,element.value);
        console.log(element);
      });
  });
  if(sessionStorage.getItem('firstName') && sessionStorage.getItem('lastName')){
    document.querySelector('#savedFirstName').innerHTML = sessionStorage.getItem('firstName');
    document.querySelector('#savedLastName').innerHTML = sessionStorage.getItem('lastName');
  }



});
