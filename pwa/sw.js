let fichiers_offline = [
    '/',
    '/style.css',
    '/app.js',
    '/img/cat.png',
    '/index.html'
]

//Installation du SW
self.addEventListener('install',(event)=>{
    self.skipWaiting();
    event.waitUntil(
        caches.open("cache-v1")
        .then(function(cache){
            console.log("Mise en cache");
            return cache.addAll(fichiers_offline);
        })
    )
// console.log('install');

})

//Activation du SW
self.addEventListener('activate',(event)=>{
    console.log("Activation");
})

//Pour chaque requête réseaux de la page
self.addEventListener('fetch',(event)=>{
    console.log("Fetch");
    event.respondWith(
        caches.match(event.request).then(function ( response){
            if(response !== undefined){
                return response;
            }else{
                return fetch(event.request)
                .then(function (response){

                    let responseClone = response.clone();
                    caches.open("v1").then(function (cache){
                        cache.put(event.request, responseClone);
                    })
                    return response;
                })
                .catch(function(){
                    return caches.match("/img/cat.png");
                })
            }
        })
    )
})