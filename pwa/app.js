(function () {

  if ("serviceWorker" in navigator) {
    navigator.serviceWorker.register("/sw.js").then(function () {
      console.log("SW inscrit");
    }).catch(function(err) {
      console.log("Fail SW : ", err);
    });

  }

  setTimeout(() => {
    const img = new Image(160);
    img.src = "/img/dog.png";
    // document.querySelector("#img-wrapper").appendChild(img);
    document.body.appendChild(img);

    // const img2 = new Image(160);
    // img2.src = "https://img.maxisciences.com/s3/frgsd/1024/requin/default_2020-07-03_2da3ffa4-3134-42e6-9fc5-31bd17a4cf9b.jpeg";
    // // document.querySelector("#img-wrapper").appendChild(img);

    // document.body.appendChild(img2);
  }, 1000);

  var online = true;
  var onlineIndicator = document.querySelector("#onlineIndicator");
  window.addEventListener('online',updateIndicator);
  window.addEventListener('offline',updateIndicator);
  
  function updateIndicator(event){
    if(online == true){
      onlineIndicator.style.background = "#FF0000";
      online =false;
    }else{
      onlineIndicator.style.background = "#008000";
      online = true;
    }

  }



  let banniere;

  window.addEventListener("beforeinstallprompt", (e) => {
    console.log("beforeinstallprompt event received");
    document.querySelector("#installBtn").classList.remove("hidden");
    // Prevent the mini-infobar from appearing on mobile
    e.preventDefault();
    // Stash the event so it can be triggered later.
    banniere = e;
  });

  document.querySelector("#installBtn").addEventListener("click", (e) => {
    // Show the install prompt
    if (banniere) {
      banniere.prompt();
      // Wait for the user to respond to the prompt
      banniere.userChoice.then((choiceResult) => {
        if (choiceResult.outcome === "accepted") {
          console.log("User accepted the install prompt");
        } else {
          console.log("User dismissed the install prompt");
        }
      });
    } else {
      console.log("beforeinstallprompt not fired");
    }
  });

})();
